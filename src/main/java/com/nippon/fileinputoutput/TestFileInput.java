/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.fileinputoutput;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nippon
 */
public class TestFileInput {

    public static void main(String[] args) {
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        Dog dog = null;
        Rectangle rectangle = null;

        try {
            File file = new File("dog.obj");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);

            dog = (Dog) ois.readObject(); //-> Object
            rectangle = (Rectangle) ois.readObject();
            System.out.println(dog);
            System.out.println(rectangle);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            System.out.println("File not found!!!");
        } catch (IOException ex) {
            Logger.getLogger(TestFileInput.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestFileInput.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (NullPointerException ex) {
                System.out.println("Null PointerException");
            } catch (IOException ex) {
                Logger.getLogger(TestFileInput.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
